local redis = require "resty.redis"
local red = redis:new()
red:set_timeout(1000) -- 1 sec
--####################### Use Socket
local ok, err = red:connect("127.0.0.1", 6379)
if not ok then
    ngx.header.content_type = "text/plain"
    ngx.say("IP not allow - Forbidden.")
    return ngx.exit(403)
end

local res, err = red:auth("YOURPASSWORD")
if not res then
    ngx.header.content_type = "text/plain"
    ngx.log(ngx.ERR, "REDIS - authen fail !!!")
    ngx.status = 403
    ngx.say("IP not allow - Forbidden.")
    return 
end

-- ### Check ip in list
--local cate, err = red:smembers('abc.com')
local ip, err = red:smembers(ngx.var.host)
if table.getn(ip) == 0 then
    --return ngx.exit(403)
    ngx.header.content_type = "text/plain"
    ngx.say("IP not allow - Forbidden.")
    ngx.status = 403
    return 
end
if err then
    ngx.log(ngx.WARN, "Redis read error: " .. err)
else
    flag = 0
    for index, l in ipairs(ip) do
        ngx.log(ngx.ERR, "Redis result: ", l, " --- ngx.var.remote_addr: ", ngx.var.remote_addr, index)
        if l == ngx.var.remote_addr then
            flag = 1
            break
        end
    end
end
if flag == 0 then
    ngx.header.content_type = "text/plain"
    ngx.status = 403
    ngx.say("IP not allow - Forbidden.")
    return 
end


